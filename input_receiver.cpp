#include "input_receiver.hpp"

#include "graph.hpp"
#include "input_distributor.hpp"

#include <mpi.h>

InputReceiver::InputReceiver() :
	graph_(new Graph(gRank)),
	pattern_(NULL),
	state_(kReadGraphVertex),
	receiver_(kInputDistributionTag),
	vertexId_(-1),
	dataToRead_(-1)
{
}

InputReceiver::~InputReceiver()
{
}

int InputReceiver::receiveByMPI()
{
	std::vector<int> *buffer = NULL;
	do {
		if (buffer) {
			delete buffer;
			buffer = NULL;
		}
		receiver_.initReceive(kDistributorProcess);
		receiver_.receiveData(&buffer);
	} while (receivePart(buffer->data(), buffer->size()));
	delete buffer;
	return processCount_;
}

bool InputReceiver::receivePart(const int *data, size_t size)
{
	const int *pointer = data;
	const int *end = data + size;
	while (pointer < end) {
		switch (state_) {
		case kReadGraphVertex:
			readGraphVertex(pointer, end);
			break;
		case kReadGraphEdgeCount:
			readGraphEdgeCount(pointer, end);
			break;
		case kReadGraphEdges:
			readGraphEdges(pointer, end);
			break;
		case kReadProcessCount:
			readProcessCount(pointer, end);
			break;
		case kReadPatternVertexCount:
			readPatternVertexCount(pointer, end);
			break;
		case kReadPatternVertex:
			readPatternVertex(pointer, end);
			break;
		case kReadPatternEdgeCount:
			readPatternEdgeCount(pointer, end);
			break;
		case kReadPatternEdges:
			readPatternEdges(pointer, end);
			break;
		case kReadProcessMapSize:
			readProcessMapSize(pointer, end);
			break;
		case kReadProcessMapData:
			readProcessMapData(pointer, end);
			break;
		default:
			printf("pointer: %lld, end = %lld\n", (long long)pointer, (long long)end);
			massert(pointer == end);
			massert(state_ == kReadEnd);
		}
	}

	return state_ != kReadEnd;
}

