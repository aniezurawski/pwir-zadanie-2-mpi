#pragma once

#include "common.hpp"
#include "buffer_sender.hpp"
#include "input_receiver.hpp"

#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <queue>
#include <set>
#include <memory.h>
#include <mpi.h>


namespace InputDistributor_detail {

struct ProcessStats {
	ProcessStats() : objects(0) {}
	int objects;
};

struct ProcessComparer {
	ProcessComparer(const std::vector<ProcessStats>& stats) :
		stats_(stats) {}

	bool operator()(int p1, int p2) {
		return stats_[p1].objects < stats_[p2].objects;
	}

private:
	const std::vector<ProcessStats> &stats_;
};

} // namespace InputDistributor_detail

class InputDistributor {
	typedef InputDistributor_detail::ProcessStats ProcessStats;
	typedef InputDistributor_detail::ProcessComparer ProcessComparer;

public:
	static const int kObjectsSoftLimit = 1 << 20;

	InputDistributor(const std::string &fileName);
	virtual ~InputDistributor();

	int distribute(int maxProcessCount);
	const InputReceiver &receiver() {
		return inputReceiver_;
	}

private:
	InputReceiver inputReceiver_;
	std::ifstream file_;
	BufferSender<int, kDistributionPoolSize, kDistributionBlockSize> bufferSender_;
	std::vector<int> vertexToProcess_;
	std::vector<ProcessStats> processStats_;
	int nextVertex_;
	std::vector<bool> verticesBitMap_;

	void resetVerticesReader(int vertexCount) {
		nextVertex_ = -1;
		verticesBitMap_.resize(vertexCount, false);
	}

	bool tryReadVertexHeader(int &vertex, int &edgeCount);
	void sendDataToProcess(int process, const std::vector<int> &data, bool forceSend = false);
	void sendDataToProcess(int process, const int *data, size_t size, bool forceSend = false);

	void skipLine() {
		std::string tmp;
		getline(file_, tmp);
	}

};

