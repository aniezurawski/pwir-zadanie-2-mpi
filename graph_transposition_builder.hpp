#pragma once

#include "graph.hpp"
#include "buffer_sender.hpp"
#include "buffer_receiver.hpp"

#include <map>

class GraphTranspositionBuilder {
public:
	const static int kTotalPoolSize = 1 << 23;
	const static int kPoolBlockSize = 1 << 13;

	GraphTranspositionBuilder(int processCount);
	void transpose(Graph &graph);

private:
	int processCount_;
	Graph *graph_;
	BufferSender<int, kTotalPoolSize, kPoolBlockSize> sender_;
	BufferReceiver<int, kPoolBlockSize> receiver_;
		int source;
	std::vector<int> edgeSource_;
	std::map<int, std::vector<int> > edges_;

	void send();
	void receive(int process);

	void addEdges();

	bool processData(int process, const std::vector<int> &data);

};

