#pragma once

template <typename T>
class Buffer {
public:
	Buffer() : buffer_(NULL), size_(0), capacity_(0) {}

	~Buffer() {
		clear();
	}

	void init(int capacity) {
		clear();
		buffer_ = new T[capacity];
		size_ = 0;
		capacity_ = capacity;
	}

	void clear() {
		if (buffer_) {
			delete[] buffer_;
			buffer_ = NULL;
		}
		size_ = 0;
		capacity_ = 0;
	}

	void setSize(int size) {
		assert_abort(size <= capacity);
		size_ = size;
	}

	int size() const {
		return size_;
	}

	int capacity() const {
		return capacity_;
	}

private:
	T *buffer_;
	int size_;
	int capacity_;

};

