CC=mpicxx
FLAGS=$(CFLAGS) -O2

HEADERS = \
		  worker.hpp \
		  input_distributor.hpp \
		  input_receiver.hpp \
		  graph.hpp \
		  graph_transposition_builder.hpp \
		  buffer.hpp \
		  common.hpp \
		  pool.hpp \
		  incremental_buffer.hpp \
		  buffer_sender.hpp \
		  buffer_receiver.hpp \
		  pattern.hpp \
		  task.hpp \
		  task_sender.hpp \
		  task_receiver.hpp \
		  request_set.hpp \
		  result_collector.hpp \
		  progress_data.hpp
OBJECTS = \
		  worker.o \
		  input_distributor.o \
		  input_receiver.o \
		  graph.o \
		  graph_transposition_builder.o \
		  common.o \
		  gpm-par.o \
		  result_collector.o

default: gpm-par.exe

%.o: %.cpp $(HEADERS)
	$(CC) $(FLAGS) -c $< -o $@


gpm-par.exe: $(OBJECTS)
	$(CC) $(OBJECTS) $(FLAGS) -o $@

gpm-seq-naive: $(OBJECTS)
	$(CC) $(OBJECTS) $(FLAGS) -o $@

clean:
	-rm -f $(OBJECTS)
	-rm -f gpm-par.exe gpm-seq-naive
