#pragma once

#include "common.hpp"

#include <vector>

class Task {
public:
	friend bool operator<(const Task &, const Task&);

	Task(int patternSize) :
		matchCount_(0),
		matches_(patternSize, 0) {}

	void addMatch(int patternVertex, int graphVertex) {
		massert(matches_[patternVertex - 1] == 0);
		matches_[patternVertex - 1] = graphVertex;
		++matchCount_;
	}

	void serialize(std::vector<int> &buffer) const {
		buffer = matches_;
	}

	void deserialize(const std::vector<int> &buffer) {
		matches_ = buffer;
		matchCount_ = 0;
		for (size_t i = 0; i < matches_.size(); ++i) {
			matchCount_ += (matches_[i] > 0);
		}
	}

	int matchCount() const {
		return matchCount_;
	}

	int match(int v) const {
		massert(v <= (int)matches_.size());
		massert(0 < v);
		return matches_[v - 1];
	}

	bool isInMatch(int graphVertex) const {
		for (size_t i = 0; i < matches_.size(); ++i) {
			if (matches_[i] == graphVertex) {
				return true;
			}
		}
		return false;
	}

private:
	int matchCount_;
	std::vector<int> matches_;

};

inline bool operator<(const Task &a, const Task &b) {
	for (size_t i = 0; i < a.matches_.size(); ++i) {
		if (a.matches_[i] != b.matches_[i]) {
			return a.matches_[i] < b.matches_[i];
		}
	}
	return false;
}

