#pragma once

#include "incremental_buffer.hpp"
#include "pool.hpp"
#include "common.hpp"

#include <mpi.h>
#include <algorithm>

template <typename T, int TotalPoolSize, int PoolBlockSize>
class BufferSender {
public:
	BufferSender(int processCount, int tag) :
		processCount_(processCount),
		tag_(tag) {
		for (int i = 0; i < TotalPoolSize / PoolBlockSize; ++i) {
			pool_.manage(new IncrementalBuffer<int>(PoolBlockSize));
		}
		for (int i = 0; i < kMaxProcessCount; ++i) {
			tmpBuffers_[i] = NULL;
		}
	}

	void addDataToSend(int process, const std::vector<T> &data, bool forceSend = false) {
		addDataToSend(process, data.data(), data.size(), forceSend);
	}

	void addDataToSend(int process, const T *data, size_t size, bool forceSend = false) {
		size_t sent = 0;
		while (sent < size) {
			IncrementalBuffer<T> *block = NULL;
			std::swap(block, tmpBuffers_[process]);
			if (!block) {
				block = waitForFreeBlock();
			}
			sent += send(
					*block,
					process,
					data + sent,
					size - sent,
					forceSend);
		}
	}

	size_t tryAddDataToSend(int process, const T *data, size_t size, bool forceSend = false) {
		size_t sent = 0;
		while (sent < size) {
			IncrementalBuffer<T> *block = NULL;
			std::swap(block, tmpBuffers_[process]);
			if (!block) {
				block = tryGetFreeBlock();
				if (!block) {
					break;
				}
			}
			sent += send(
					*block,
					process,
					data + sent,
					size - sent,
					forceSend);
		}
		return sent;
	}

	void forceSend(int process) {
		IncrementalBuffer<T> *block = NULL;
		std::swap(block, tmpBuffers_[process]);
		if (!block) {
			return;
		}
		send(*block, process, NULL, 0, true);
	}

	void waitForAll() {
		massert(MPI_Waitall(
					sendingRequests_.size(),
					sendingRequests_.data(),
					MPI_STATUSES_IGNORE) == MPI_SUCCESS);
		pool_.releaseAll();
	}

	const std::vector<MPI_Request> &getRequests() const {
		return sendingRequests_;
	}

	const std::vector<int> &destProcess() const {
		return destProcess_;
	}

	void reportEndRequest(int idx, const MPI_Status &) {
		endedRequests_.push_back(idx);
	}

	void deleteEndedRequests() {
		std::sort(endedRequests_.begin(), endedRequests_.end());
		while (!endedRequests_.empty()) {
			deleteRequest(endedRequests_.back());
			endedRequests_.pop_back();
		}
	}

private:
	int processCount_;
	int tag_;
	Pool<IncrementalBuffer<T> > pool_;
	std::vector<IncrementalBuffer<T>*> sendingBuffers_;
	std::vector<MPI_Request> sendingRequests_;
	std::vector<int> destProcess_;
	IncrementalBuffer<T> *tmpBuffers_[kMaxProcessCount];
	std::vector<int> endedRequests_;

	size_t send(IncrementalBuffer<T> &block,
			int process,
			const T *data,
			size_t size,
			bool forceSend) {
		size_t chunkSize = std::min(size, block.freeSpace());
		block.push(data, chunkSize);
		
		if (block.full() || forceSend) {
			MPI_Request req;
			massert(MPI_Isend(
						block.data(),
						block.size(),
						MPI_INT,
						process,
						tag_,
						MPI_COMM_WORLD,
						&req) == MPI_SUCCESS);
			sendingBuffers_.push_back(&block);
			sendingRequests_.push_back(req);
			destProcess_.push_back(process);
		} else {
			tmpBuffers_[process] = &block;
		}

		return chunkSize;
	}

	IncrementalBuffer<T> *waitForFreeBlock() {
		IncrementalBuffer<T> *block = pool_.acquire();
		if (block) {
			return block;
		}
		std::vector<int> reqIdx(sendingRequests_.size());
		massert(!reqIdx.empty());
		int count;
		massert(MPI_Waitsome(
					sendingRequests_.size(),
					sendingRequests_.data(),
					&count,
					reqIdx.data(),
					MPI_STATUSES_IGNORE) == MPI_SUCCESS);
		massert(count != MPI_UNDEFINED);
		reqIdx.resize(count);
		std::sort(reqIdx.begin(), reqIdx.end());
		for (size_t i = count; i > 0; --i) {
			deleteRequest(reqIdx[i - 1]);
		}
		block = pool_.acquire();
		massert(block);
		return block;
	}
	
	IncrementalBuffer<T> *tryGetFreeBlock() {
		IncrementalBuffer<T> *block = pool_.acquire();
		if (block) {
			return block;
		}
		std::vector<int> reqIdx(sendingRequests_.size());
		massert(!reqIdx.empty());
		int count;
		massert(MPI_Testsome(
					sendingRequests_.size(),
					sendingRequests_.data(),
					&count,
					reqIdx.data(),
					MPI_STATUSES_IGNORE) == MPI_SUCCESS);
		massert(count != MPI_UNDEFINED);
		reqIdx.resize(count);
		std::sort(reqIdx.begin(), reqIdx.end());
		for (size_t i = count; i > 0; --i) {
			deleteRequest(reqIdx[i - 1]);
		}
		return pool_.acquire();
	}

	void deleteRequest(int i) {
		IncrementalBuffer<T> *block = sendingBuffers_[i];
		std::swap(sendingBuffers_[i], sendingBuffers_.back());
		std::swap(sendingRequests_[i], sendingRequests_.back());
		std::swap(destProcess_[i], destProcess_.back());
		sendingBuffers_.pop_back();
		sendingRequests_.pop_back();
		destProcess_.pop_back();
		block->clear();
		pool_.release(block);
	}
};

