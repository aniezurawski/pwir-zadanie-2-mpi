#pragma once

#include "common.hpp"

#include <vector>
#include <set>
#include <cstdio>
#include <stack>

class Pattern {
	friend struct PermuteComparer;

	struct PermuteComparer {
		PermuteComparer(Pattern *pattern) : pattern(pattern) {}

		bool operator()(int a, int b) {
			return pattern->out_[a].size() + pattern->in_[a].size()
				> pattern->out_[b].size() + pattern->in_[b].size();
		}

		Pattern *pattern;
	};

public:
	static const int kMaxVertexCount = 10;

	Pattern(int vertexCount) :
		vertexCount_(vertexCount),
		permutation_(vertexCount) {}

	int size() const {
		return vertexCount_;
	}

	const std::vector<int> in(int v) const {
		return in_[v - 1];
	}

	const std::vector<int> out(int v) const {
		return out_[v - 1];
	}

	void addEdge(int a, int b) {
		out_[a - 1].push_back(b);
		in_[b - 1].push_back(a);
	}

	int newId(int originalId) const {
		return permutation_[originalId - 1] + 1;
	}

	void permute() {
		std::vector<int> order;
		std::vector<int> newOut[kMaxVertexCount];
		std::vector<int> newIn[kMaxVertexCount];

		int minEdges = 1000;
		int bestVertex = 0;
		for (int i = 0; i < vertexCount_; ++i) {
			std::vector<bool> counted(vertexCount_, false);
			int edges = 0;
			for (size_t j = 0; j < out_[i].size(); ++j) {
				if (!counted[out_[i][j]]) {
					++edges;
					counted[out_[i][j]] = true;
				}
			}
			for (size_t j = 0; j < in_[i].size(); ++j) {
				if (!counted[in_[i][j]]) {
					++edges;
					counted[in_[i][j]] = true;
				}
			}
			if (edges < minEdges) {
				minEdges = edges;
				bestVertex = i;
			}
		}

		std::vector<bool> visited(vertexCount_, false);
		std::stack<int> stack;
		stack.push(bestVertex);
		visited[bestVertex] = true;
		while (!stack.empty()) {
			int vertex =  stack.top();
			stack.pop();
			order.push_back(vertex);
			for (size_t i = 0; i < out_[vertex].size(); ++i) {
				int w = out_[vertex][i] - 1;
				if (!visited[w]) {
					visited[w] = true;
					stack.push(w);
				}
			}
			for (size_t i = 0; i < in_[vertex].size(); ++i) {
				int w = in_[vertex][i] - 1;
				if (!visited[w]) {
					visited[w] = true;
					stack.push(w);
				}
			}
		}

		/*std::multiset<int, PermuteComparer> reachable(this);
		size_t max = 0;
		int best = 0;
		for (int i = 0; i < vertexCount_; ++i) {
			if (out_[i].size() + in_[i].size() > max) {
				max = out_[i].size() + in_[i].size();
				best = i;
			}
		}
		visited[best] = true;
		reachable.insert(best);
		while (!reachable.empty()) {
			int v = *reachable.begin();
			reachable.erase(reachable.begin());
			order.push_back(v);
			//printf("%d> ODRER PUSH %d, reach:", gRank, v);
			for (size_t i = 0; i < out_[v].size(); ++i) {
				int w = out_[v][i] - 1;
				if (!visited[w]) {
					//printf(" %d", w);
					visited[w] = true;
					reachable.insert(w);
				}
			}
			for (size_t i = 0; i < in_[v].size(); ++i) {
				int w = in_[v][i] - 1;
				if (!visited[w]) {
					//printf(" %d", w);
					visited[w] = true;
					reachable.insert(w);
				}
			}
			//printf("\n");
		}*/

		for (int i = 0; i < vertexCount_; ++i) {
			//printf("%d> %d <- %d\n", gRank, i, order[i]);
			permutation_[order[i]] = i;
			newOut[i] = out_[order[i]];
			newIn[i] = in_[order[i]];
		}

		for (int i = 0; i < vertexCount_; ++i) {
			out_[i].clear();
			for (size_t j = 0; j < newOut[i].size(); ++j) {
				out_[i].push_back(newId(newOut[i][j]));
			}
			in_[i].clear();
			for (size_t j = 0; j < newIn[i].size(); ++j) {
				in_[i].push_back(newId(newIn[i][j]));
			}
		}
	}

private:
	int vertexCount_;
	std::vector<int> out_[kMaxVertexCount];
	std::vector<int> in_[kMaxVertexCount];
	std::vector<int> permutation_;

};

