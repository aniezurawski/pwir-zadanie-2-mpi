#pragma once

#include "common.hpp"

#include <mpi.h>
#include <algorithm>

template <typename T, int BlockSize>
class BufferReceiver {
public:
	BufferReceiver(int tag) : tag_(tag) {}

	int receiveData(std::vector<T> **buffer) {
		if (!endedBuffers_.empty()) {
			*buffer = endedBuffers_.back();
			int source = endedSources_.back();
			endedBuffers_.pop_back();
			endedSources_.pop_back();
			return source;
		}
		MPI_Status status;
		int idx;
		massert(MPI_Waitany(
					requests_.size(),
					requests_.data(),
					&idx,
					&status) == MPI_SUCCESS);
		*buffer = buffers_[idx];
		deleteRequest(idx);
		int count;
		MPI_Get_count(&status, MpiType<T>::value(), &count);
		(*buffer)->resize(count);
		return status.MPI_SOURCE;
	}

	int tryReceiveData(std::vector<T> **buffer) {
		if (!endedBuffers_.empty()) {
			*buffer = endedBuffers_.back();
			int source = endedSources_.back();
			endedBuffers_.pop_back();
			endedSources_.pop_back();
			return source;
		}
		if (requests_.empty()) {
			return -1;
		}
		MPI_Status status;
		int idx;
		int flag;
		massert(MPI_Testany(
					requests_.size(),
					requests_.data(),
					&idx,
					&flag,
					&status) == MPI_SUCCESS);
		if (!flag) {
			return -1;
		}
		*buffer = buffers_[idx];
		deleteRequest(idx);
		int count;
		MPI_Get_count(&status, MpiType<T>::value(), &count);
		(*buffer)->resize(count);
		return status.MPI_SOURCE;
	}

	void initReceive(int process) {
		buffers_.push_back(new std::vector<T>(BlockSize));
		requests_.push_back(MPI_Request());
		massert(MPI_Irecv(
					buffers_.back()->data(),
					BlockSize,
					MpiType<T>::value(),
					process,
					tag_,
					MPI_COMM_WORLD,
					&requests_.back()) == MPI_SUCCESS);
	}

	const std::vector<MPI_Request> &getRequests() const {
		return requests_;
	}

	void reportEndRequest(int idx, MPI_Status &status) {
		processEndedRequest(idx, status);
		endedRequests_.push_back(idx);
	}

	void deleteEndedRequests() {
		std::sort(endedRequests_.begin(), endedRequests_.end());
		while (!endedRequests_.empty()) {
			deleteRequest(endedRequests_.back());
			endedRequests_.pop_back();
		}
	}

private:
	int tag_;
	std::vector<MPI_Request> requests_;
	std::vector<std::vector<T> *> buffers_;
	std::vector<std::vector<T> *> endedBuffers_;
	std::vector<int> endedSources_;
	std::vector<int> endedRequests_;

	void deleteRequest(int idx) {
		std::swap(buffers_[idx], buffers_.back());
		std::swap(requests_[idx], requests_.back());
		buffers_.pop_back();
		requests_.pop_back();
	}

	void processEndedRequest(int idx, MPI_Status &status) {
		int count;
		MPI_Get_count(&status, MpiType<T>::value(), &count);
		buffers_[idx]->resize(count);
		endedBuffers_.push_back(buffers_[idx]);
		endedSources_.push_back(status.MPI_SOURCE);
	}

};

