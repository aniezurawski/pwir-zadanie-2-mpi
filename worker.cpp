#include "worker.hpp"

#include "request_set.hpp"

#include <mpi.h>

Worker::Worker(int processCount, int procId, const Graph &graph, const Pattern &pattern) :
	processCount_(processCount),
	procId_(procId),
	graph_(graph),
	pattern_(pattern),
	idleTokenReq_(NULL)
{
	taskSenders_.resize(pattern_.size(), NULL);
	for (int i = 1; i <= pattern_.size() - 1; ++i) {
		taskSenders_[i] = new TaskSender<kTaskPoolSize, kTaskBlockSize>(
				processCount,
				kTaskLevelTag[i]);
	}
	taskReceivers_.resize(pattern_.size(), NULL);
	for (int i = 1; i <= pattern_.size() - 1; ++i) {
		taskReceivers_[i] = new TaskReceiver<kTaskBlockSize>(
				processCount,
				pattern_.size(),
				kTaskLevelTag[i],
				procId_);
	}

	tasks_.resize(pattern_.size(), TaskInfo(processCount));
	tasks_[0].set(new Task(pattern_.size()));
}

Worker::~Worker()
{
	for (int i = 1; i <= pattern_.size() - 1; ++i) {
		delete taskSenders_[i];
	}
	for (int i = 1; i <= pattern_.size() - 1; ++i) {
		delete taskReceivers_[i];
	}
}

void Worker::work(std::vector<Task> &result)
{
	//printf("START\n");
	matches_ = &result;
	matches_->reserve(5000);
	bool hasIdleToken = (procId_ == kDistributorProcess);
	bool sendingIdleToken = false;
	int idleTokenCounter = 0;
	bool done = false;
	while (!done) {
		bool taskFound = false;
		int level;
		for (level = pattern_.size() - 1; level >= 0; --level) {
			if (!tryReceiveTask(level)) {
				if (level + 1 < pattern_.size()) {
					taskSenders_[level + 1]->forceSend();
				}
				continue;
			}
			if (!trySendTask(level)) {
				if (level + 1 < pattern_.size()) {
					taskSenders_[level + 1]->forceSend();
				}
				break;
			}
			taskFound = true;
			processTask(level);
			break;
		}
		if (taskFound) {
			continue;
		}

		bool idle = level <= -1;
		idleTokenCounter = 0;
		do {
			RequestSet reqSet;
			
			// Receive task requests
			int recvGroupFirst = -1;
			int recvGroupLast = -1;
			int minRecvLevel = std::max(level + 1, 1);
			for (int l = minRecvLevel; l < pattern_.size(); ++l) {
				recvGroupLast = reqSet.addGroup(taskReceivers_[l]->getRequests());
				recvGroupFirst = (recvGroupFirst == -1) ? recvGroupLast : recvGroupFirst;
			}
			int recvReqs = reqSet.size();
			
			// Send task requests
			int sendGroupFirst = -1;
			int sendGroupLast = -1;
			int minSendLevel = std::max(level + 1, 1);
			std::vector<int> dest;
			for (int l = minSendLevel; l < pattern_.size(); ++l) {
				sendGroupLast = reqSet.addGroup(taskSenders_[l]->getRequests());
				sendGroupFirst = (sendGroupFirst == -1) ? sendGroupLast : sendGroupFirst;
				dest.insert(
						dest.end(),
						taskSenders_[l]->destProcess().begin(),
						taskSenders_[l]->destProcess().end());
			}
			int sendReqs = reqSet.size() - recvReqs;
			idle = idle && (sendReqs == 0);
			/*printf("%d> Send reqs: %d:", gRank, sendReqs);
			for (size_t i = 0; i < dest.size(); ++i) {
				printf(" %d", dest[i]);
			}
			printf("\n");*/

			// Idle requests
			int idleRecvGroup = -1;
			int idleSendGroup = -1;
			if (idle) {
				if (hasIdleToken || sendingIdleToken) {
					idleSendGroup = reqSet.addGroup(getIdleTokenSendReq(idleTokenCounter + 1));
					hasIdleToken = false;
					sendingIdleToken = true;
					//printf("%d> TRY SEND TOKEN %d\n", gRank, idleTokenCounter + 1);
				} else {
					idleRecvGroup = reqSet.addGroup(getIdleTokenRecvReq());
					//printf("%d> TRY RECV TOKEN\n", gRank);
				}
			}

			// Wait
			std::vector<MPI_Status> statuses(reqSet.size());
			std::vector<int> reqIndexes(reqSet.size());
			int count = 1;
			massert(MPI_Waitany(
						reqSet.size(),
						reqSet.data(),
						//&count,
						reqIndexes.data(),
						statuses.data()) == MPI_SUCCESS);
			//printf("%d> Wake up\n", gRank);
			for (int i = 0; i < count; ++i) {
				massert(statuses[i].MPI_ERROR == MPI_SUCCESS);
				std::pair<int, int> groupAndIndex = reqSet.getGroupAndIndex(reqIndexes[i]);
				int group = groupAndIndex.first;
				int index = groupAndIndex.second;
				if (recvGroupFirst <= group && group <= recvGroupLast) {
					taskReceivers_[minRecvLevel + group - recvGroupFirst]->reportEndRequest(
							index, statuses[i]);
					idle = false;
				} else if (sendGroupFirst <= group && group <= sendGroupLast) {
					taskSenders_[minSendLevel + group - sendGroupFirst]->reportEndRequest(
							index, statuses[i]);
					idle = false;
				} else if (group == idleRecvGroup) {
					hasIdleToken = true;
					//printf("%d> RECV\n", gRank);
					if (count > 1) {
						continue;
					}
					//printf("%d> GOT TOKEN %d, had %d\n", gRank, idleTokenBuffer_, idleTokenCounter);
					if (idleTokenBuffer_ < processCount_
							|| idleTokenCounter + processCount_ == idleTokenBuffer_) {
						//printf("%d> Update\n", gRank);
						idleTokenCounter = idleTokenBuffer_;
					} else {
						//printf("%d> Reset\n", gRank);
						idleTokenCounter = 0;
					}
					if (idleTokenCounter >= 2 * processCount_) {
						idle = false;
						done = true;
						if (idleTokenCounter >= 3 * processCount_ - 1) {
							hasIdleToken = false;
						}
					}
					delete idleTokenReq_;
					idleTokenReq_ = NULL;
				} else if (group == idleSendGroup) {
					//printf("%d> SENT\n", gRank);
					sendingIdleToken = false;
					delete idleTokenReq_;
					idleTokenReq_ = NULL;
				}
			}

			// Delete ended requests
			for (int l = minRecvLevel; l < pattern_.size(); ++l) {
				taskReceivers_[l]->deleteEndedRequests();
			}
			for (int l = minSendLevel; l < pattern_.size(); ++l) {
				taskSenders_[l]->deleteEndedRequests();
			}
		} while (idle);
	}
	
	//printf("%d> / ***** FINISHED ***** /\n", procId_);
	
	// Close requests
	RequestSet reqSet;
	for (int l = 1; l < pattern_.size(); ++l) {
		std::vector<MPI_Request> req = taskReceivers_[l]->getRequests();
		for (size_t i = 0; i < req.size(); ++i) {
			massert(MPI_Cancel(req.data() + i) == MPI_SUCCESS);
		}
		reqSet.addGroup(req);
	}
	for (int l = 1; l < pattern_.size(); ++l) {
		reqSet.addGroup(taskSenders_[l]->getRequests());
	}
	if (hasIdleToken) {
		reqSet.addGroup(getIdleTokenSendReq(idleTokenCounter + 1));
	}

	massert(MPI_Waitall(
				reqSet.size(),
				reqSet.data(),
				MPI_STATUSES_IGNORE) == MPI_SUCCESS);

	delete idleTokenReq_;

	//printf("%d> / ***** CLOSED ***** /\n", procId_);

	//std::sort(matches_->begin(), matches_->end());
}

void Worker::processTask(int level)
{
	TaskInfo &taskInfo = tasks_[level];
	massert(taskInfo.task());
	/*printf("%d> level %d:", gRank, level);
	for (int i = 1; i <= pattern_.size(); ++i) {
		printf(" %d", taskInfo.task()->match(i));
	}
	printf("\n");*/
	massert(taskInfo.task()->matchCount() == level);
	massert(!taskInfo.taskToSend());

	const Task &task = *(taskInfo.task());
	Task *newTask = NULL;

	if (level == 0) {
		if (taskInfo.progress() < graph_.partSize()) {
			int vertex = graph_.vertex(taskInfo.progress());
			newTask = new Task(task);
			newTask->addMatch(1, vertex);
			taskInfo.incProgress();
		}
	} else {
		newTask = findNewMatch(taskInfo, level);
	}

	if (!newTask) {
		taskInfo.clear();
		return;
	}

	if (level < pattern_.size() - 1) {
		taskInfo.setTaskToSend(newTask);
		addDestinationProcesses(*newTask, taskInfo);
	} else {
		matches_->push_back(*newTask);
		delete newTask;
	}
}

void Worker::addDestinationProcesses(const Task &task, TaskInfo &taskInfo)
{
	bool toAll = true;
	std::vector<int> dest;
	int vertex = task.matchCount() + 1;
	for (size_t i = 0; i < pattern_.out(vertex).size(); ++i) {
		std::set<int> destTmp;
		int other = pattern_.out(vertex)[i];
		if (other > vertex) {
			continue;
		}
		int graphVertex = task.match(other);
		if (graph_.isForeign(graphVertex)) {
			continue;
		}
		//printf("%d> graphVertex = %d\n", gRank, graphVertex);
		for (const int *e = graph_.inBegin(graphVertex); e != graph_.inEnd(graphVertex); ++e) {
			//printf("%d> insert %d\n", gRank, *e);
			destTmp.insert(graph_.getVertexProcess(*e));
		}
		if (toAll == true) {
			toAll = false;
			dest.insert(dest.end(), destTmp.begin(), destTmp.end());
		} else {
			size_t i = 0;
			while (i < dest.size()) {
				if (destTmp.count(dest[i]) == 0) {
					std::swap(dest[i], dest.back());
					dest.pop_back();
				} else {
					++i;
				}
			}
		}
	}
	
	for (size_t i = 0; i < pattern_.in(vertex).size(); ++i) {
		std::set<int> destTmp;
		int other = pattern_.in(vertex)[i];
		if (other > vertex) {
			continue;
		}
		int graphVertex = task.match(other);
		if (graph_.isForeign(graphVertex)) {
			continue;
		}
		//printf("%d> graphVertex = %d\n", gRank, graphVertex);
		for (const int *e = graph_.outBegin(graphVertex); e != graph_.outEnd(graphVertex); ++e) {
			//printf("%d> insert %d\n", gRank, *e);
			destTmp.insert(graph_.getVertexProcess(*e));
		}
		if (toAll == true) {
			toAll = false;
			dest.insert(dest.end(), destTmp.begin(), destTmp.end());
		} else {
			size_t i = 0;
			while (i < dest.size()) {
				if (destTmp.count(dest[i]) == 0) {
					std::swap(dest[i], dest.back());
					dest.pop_back();
				} else {
					++i;
				}
			}
		}
	}
	//printf("%d> Send to:", gRank);
	if (toAll) {
		for (int i = 0; i < processCount_; ++i) {
			taskInfo.addDestinationProcess(i);
			//printf(" %d", i);
		}
	} else for (std::vector<int>::iterator it = dest.begin(); it != dest.end(); ++it) {
		taskInfo.addDestinationProcess(*it);
		//printf(" %d", *it);
	}
	//printf("\n");
}

bool Worker::trySendTask(int level)
{
	TaskInfo &task = tasks_[level];
	if (!task.taskToSend()) {
		return true;
	}
	while (task.destinationProcessExists()) {
		int dest = task.nextDestinationProcess();
		if (dest == procId_) {
			if (!tasks_[level + 1].task()) {
				Task *newTask = new Task(*(task.taskToSend()));
				tasks_[level + 1].set(newTask);
			} else {
				return false;
			}
		} else if (!taskSenders_[level + 1]->tryAddTask(
					task.nextDestinationProcess(),
					*(task.taskToSend()))) {
			return false;
		}
		task.removeDestinationProcess();
	}
	task.clearTaskToSend();
	return true;
}

bool Worker::tryReceiveTask(int level)
{
	if (!tasks_[level].task() && level > 0) {
		tasks_[level].set(taskReceivers_[level]->tryReceiveTask());
	}		
	return tasks_[level].task() != NULL;
}

Task *Worker::findNewMatch(TaskInfo &taskInfo, int level)
{
	const Task &task = *(taskInfo.task());
	/*printf("%d> Task:", gRank);
	for (int i = 1; i <= pattern_.size(); ++i) {
		printf(" %d", task.match(i));
	}
	printf("\n");*/
	if (!taskInfo.progressData()) {
		taskInfo.setProgressData(new ProgressData(pattern_, graph_, task));
	}
	int vertex = taskInfo.progressData()->findNextMatch();
	if (vertex < 0) {
		delete taskInfo.progressData();
		taskInfo.setProgressData(NULL);
		return NULL;
	}
	Task *newTask = new Task(task);
	newTask->addMatch(task.matchCount() + 1, vertex);
	/*if (newTask->matchCount()) {// == pattern_.size()) {
		printf("%d> New match:", gRank);
		for (int i = 1; i <= pattern_.size(); ++i) {
			printf(" %d", newTask->match(i));
		}
		printf("\n");
	}*/
	return newTask;
}

std::vector<MPI_Request> Worker::getIdleTokenRecvReq()
{
	if (!idleTokenReq_) {
		idleTokenReq_ = new MPI_Request();
		massert(MPI_Irecv(
					&idleTokenBuffer_,
					1,
					MPI_INT,
					(procId_ + processCount_ - 1) % processCount_,
					kIdleTokenTag,
					MPI_COMM_WORLD,
					idleTokenReq_) == MPI_SUCCESS);
	}
	return std::vector<MPI_Request>(1, *idleTokenReq_);
}

std::vector<MPI_Request> Worker::getIdleTokenSendReq(int token)
{
	if (!idleTokenReq_) {
		idleTokenBuffer_ = token;
		idleTokenReq_ = new MPI_Request();
		massert(MPI_Isend(
					&idleTokenBuffer_,
					1,
					MPI_INT,
					(procId_ + 1) % processCount_,
					kIdleTokenTag,
					MPI_COMM_WORLD,
					idleTokenReq_) == MPI_SUCCESS);
	}
	return std::vector<MPI_Request>(1, *idleTokenReq_);
}

