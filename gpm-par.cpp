#include "common.hpp"
#include "input_distributor.hpp"
#include "input_receiver.hpp"
#include "graph.hpp"
#include "graph_transposition_builder.hpp"
#include "worker.hpp"
#include "result_collector.hpp"

#include <mpi.h>

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);

	if (argc != 3) {
		abort("usage: %s <in-file> <out-file>", argv[0]);
	}

	FILE *output = NULL;

	int myRank;
	int processCount;
	MPI_Comm_size(MPI_COMM_WORLD, &processCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	gRank = myRank;

	if (myRank == kDistributorProcess) {
		output = fopen(argv[2], "w");
		if (!output) {
			abort("Cannot open file %s", argv[2]);
		}
	}

	double startTime = MPI_Wtime();

	Graph *graph;
	Pattern *pattern;
	if (myRank == kDistributorProcess) {
		InputDistributor distributor(argv[1]);
		processCount = distributor.distribute(processCount);
		graph = distributor.receiver().graph();
		pattern = distributor.receiver().pattern();
	} else {
		InputReceiver receiver;
		processCount = receiver.receiveByMPI();
		graph = receiver.graph();
		pattern = receiver.pattern();
	}

	//printf("%d> processes: %d\n", gRank, processCount);

	if (myRank >= processCount) {
		MPI_Finalize();
		return 0;
	}
	

	{
		GraphTranspositionBuilder transpositionBuilder(processCount);
		transpositionBuilder.transpose(*graph);
	}

	graph->buildForeignVertices();

	//MPI_Barrier(MPI_COMM_WORLD);
	double distributionEndTime = 0;
	if (myRank == 0) {
		distributionEndTime = MPI_Wtime();
		printf("Distribution time[s]: %.3f\n", distributionEndTime - startTime);
	}
	
	/*for (size_t i = 0; i < graph->vertices().size(); ++i) {
		int v = graph->vertices()[i].id;
		printf("%d> vertex %d:", myRank, graph->vertices()[i].id);
		for (int *e = graph->outBegin(v); e != graph->outEnd(v); ++e) {
			printf(" %d", *e);
		}
		printf(" | ");
		for (int *e = graph->inBegin(v); e != graph->inEnd(v); ++e) {
			printf(" %d", *e);
		}
		printf("\n");
	}
	printf("%d> process tab:", myRank);
	for (int i = 1; i <= graph->allVertexCount(); ++i) {
		printf(" %d", graph->getVertexProcess(i));
	}
	printf("\n");

	for (int i = 1; i <= pattern->size(); ++i) {
		printf("%d> pattern %d:", myRank, i);
		for (size_t j = 0; j < pattern->out(i).size(); ++j) {
			printf(" %d", pattern->out(i)[j]);
		}
		printf(" | ");
		for (size_t j = 0; j < pattern->in(i).size(); ++j) {
			printf(" %d", pattern->in(i)[j]);
		}
		printf("\n");
	}*/

	pattern->permute();
	std::vector<Task> matches;
	Worker *worker = new Worker(processCount, myRank, *graph, *pattern);
	worker->work(matches);
	delete worker;
	delete graph;

	//MPI_Barrier(MPI_COMM_WORLD);
	ResultCollector *collector = new ResultCollector(processCount, *pattern, myRank, matches);
	collector->collect(output);
	delete collector;
	delete pattern;

	double computationsEndTime = 0;
	if (myRank == 0) {
		computationsEndTime = MPI_Wtime();
		printf("Computations time[s]: %.3f\n", computationsEndTime - distributionEndTime);
	}

	//MPI_Barrier(MPI_COMM_WORLD);
	/*double resultCollectingEndTime = 0;
	if (myRank == 0) {
		resultCollectingEndTime = MPI_Wtime();
		printf("Result collecting time[s]: %.3f\n", resultCollectingEndTime - computationsEndTime);
	}*/

	if (output) {
		fclose(output);
	}

	MPI_Finalize();

	return 0;
}

