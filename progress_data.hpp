#pragma once

#include "task.hpp"
#include "graph.hpp"
#include "pattern.hpp"

#include <set>

namespace ProgressData_detail {

struct EdgeList {
	EdgeList(const int *begin, const int *end, int vertex) :
		begin(begin), end(end), vertex(vertex) {
	}
	const int *begin;
	const int *end;
	int vertex;
};

struct EdgeListComparer {
	bool operator()(const EdgeList &a, const EdgeList &b) {
		return *a.begin < *b.begin;
	}
};

} // namespace ProgressData_detail

class ProgressData {
public:
	typedef ProgressData_detail::EdgeList EdgeList;
	typedef ProgressData_detail::EdgeListComparer EdgeListComparer;

	ProgressData(const Pattern &pattern, const Graph &graph, const Task &task) :
		pattern_(pattern),
		graph_(graph),
		task_(task),
		noMatch_(false)
	{
		int patternVertex = task.matchCount() + 1;
		/*printf("%d> %d: out:", gRank, patternVertex);
		for (size_t i = 0; i < pattern_.out(patternVertex).size(); ++i) {
			int other = pattern_.out(patternVertex)[i];
			printf(" %d", other);
		}
		printf(" in:");
		for (size_t i = 0; i < pattern_.in(patternVertex).size(); ++i) {
			int other = pattern_.in(patternVertex)[i];
			printf(" %d", other);
		}
		printf("\n");*/
		for (size_t i = 0; i < pattern_.out(patternVertex).size(); ++i) {
			int other = pattern_.out(patternVertex)[i];
			massert(other != patternVertex);
			if (other > patternVertex) {
				continue;
			}
			int graphVertex = task.match(other);
			if (!graph_.isIncident(graphVertex)) {
				noMatch_ = true;
				return;
			}
			if (graph_.inBegin(graphVertex) == graph_.inEnd(graphVertex)) {
				noMatch_ = true;
				return;
			}
			edgeListSet_.insert(EdgeList(
						graph_.inBegin(graphVertex),
						graph_.inEnd(graphVertex),
						graphVertex));
		}
		for (size_t i = 0; i < pattern_.in(patternVertex).size(); ++i) {
			int other = pattern_.in(patternVertex)[i];
			massert(other != patternVertex);
			if (other > patternVertex) {
				continue;
			}
			int graphVertex = task.match(other);
			if (!graph_.isIncident(graphVertex)) {
				noMatch_ = true;
				return;
			}
			if (graph_.outBegin(graphVertex) == graph_.outEnd(graphVertex)) {
				noMatch_ = true;
				return;
			}
			edgeListSet_.insert(EdgeList(
						graph_.outBegin(graphVertex),
						graph_.outEnd(graphVertex),
						graphVertex));
		}
	}

	int findNextMatch()
	{
		if (noMatch_) {
			return -1;
		}
		massert(!edgeListSet_.empty());
		/*printf("%d>", gRank);
		for (std::multiset<EdgeList, EdgeListComparer>::iterator it = edgeListSet_.begin();
				it != edgeListSet_.end(); ++it) {
			printf(" %d:(", it->vertex);
			for (const int *a = it->begin; a != it->end; ++a) {
				printf(" %d", *a);
			}
			printf(")");
		}
		printf("\n");*/
		int ret = -1;
		while (ret < 0) {
			if (*(edgeListSet_.begin()->begin) == *(edgeListSet_.rbegin()->begin)
					&& !task_.isInMatch(*(edgeListSet_.begin()->begin))
					&& !graph_.isForeign(*(edgeListSet_.begin()->begin))) {
				ret = *(edgeListSet_.begin()->begin);
			}
			EdgeList minEdgeList = *edgeListSet_.begin();
			edgeListSet_.erase(edgeListSet_.begin());
			++minEdgeList.begin;
			if (minEdgeList.begin == minEdgeList.end) {
				if (ret < 0) {
					return -1;
				}
				noMatch_ = true;
				return ret;
			}
			edgeListSet_.insert(minEdgeList);
		}
		return ret;
	}

private:
	const Pattern &pattern_;
	const Graph &graph_;
	const Task &task_;
	std::multiset<EdgeList, EdgeListComparer> edgeListSet_;
	bool noMatch_;

};

