#include "result_collector.hpp"

#include "request_set.hpp"

ResultCollector::ResultCollector(
		int processCount,
		const Pattern &pattern,
		int rank,
		const std::vector<Task> &resultPart) :
	processCount_(processCount),
	rank_(rank),
	ended_(1),
	resultPartPos_(0),
	pattern_(pattern),
	resultPart_(resultPart),
	sender_(processCount, kResultCollectionTag),
	receiver_(processCount, pattern_.size(), kResultCollectionTag, rank)
{

}

void ResultCollector::collect(FILE *output)
{
	/* Sortowane wyjscie */
	/*
	if (rank_ == kDistributorProcess) {
		for (int i = 0; i < processCount_; ++i) {
			Task *task = receiveFrom(i);
			if (task) {
				tasks_.insert(std::make_pair(task, i));
			}
		}

		while (!tasks_.empty()) {
			std::pair<Task *, int> t = *tasks_.begin();
			tasks_.erase(tasks_.begin());
			for (int i = 1; i < pattern_.size(); ++i) {
				fprintf(output, "%d ", t.first->match(pattern_.newId(i)));
			}
			fprintf(output, "%d\n", t.first->match(pattern_.newId(pattern_.size())));
			Task *task = receiveFrom(t.second);
			if (task) {
				tasks_.insert(std::make_pair(task, t.second));
			}
			delete t.first;
		}
	} else {
		for (size_t i = 0; i < resultPart_.size(); ++i) {
			sender_.addTask(
					kDistributorProcess,
					resultPart_[i],
					false);
		}
		sender_.addTask(
				kDistributorProcess,
				Task(pattern_.size()),
				true);
	}
	*/
	if (rank_ == kDistributorProcess) {
		while (Task *task = receive()) {
			for (int i = 1; i < pattern_.size(); ++i) {
				fprintf(output, "%d ", task->match(pattern_.newId(i)));
			}
			fprintf(output, "%d\n", task->match(pattern_.newId(pattern_.size())));
			delete task;
		}
		
		std::vector<MPI_Request> req = receiver_.getRequests();
		for (size_t i = 0; i < req.size(); ++i) {
			massert(MPI_Cancel(req.data() + i) == MPI_SUCCESS);
		}
		massert(MPI_Waitall(
					req.size(),
					req.data(),
					MPI_STATUSES_IGNORE) == MPI_SUCCESS);
	} else {
		for (size_t i = 0; i < resultPart_.size(); ++i) {
			sender_.addTask(
					kDistributorProcess,
					resultPart_[i],
					false);
		}
		sender_.addTask(
				kDistributorProcess,
				Task(pattern_.size()),
				true);
		std::vector<MPI_Request> req = sender_.getRequests();
		massert(MPI_Waitall(
					req.size(),
					req.data(),
					MPI_STATUSES_IGNORE) == MPI_SUCCESS);
	}
}

Task *ResultCollector::receive()
{
	if (resultPartPos_ < resultPart_.size()) {
		return new Task(resultPart_[resultPartPos_++]);
	}

	if (processCount_ == 1) {
		return NULL;
	}

	do {
		Task *task = receiver_.receiveTask();
		if (task->matchCount() > 0) {
			return task;
		}
		delete task;
		++ended_;
		if (ended_ >= processCount_) {
			return NULL;
		}
	} while (true);
}

