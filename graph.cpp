#include "graph.hpp"

#include "common.hpp"

#include <algorithm>

Graph::Graph(int rank) :
	rank_(rank),
	vertexToProcess_(kMaxVertexCount, -1),
	currentIdx_(-1),
	outEdgeCount_(0)
{
	vertices_.reserve(10000);
	edges_.reserve(1 << 25);
}

Graph::~Graph()
{

}

void Graph::addVertex(int id)
{
	idToIdx_[id] = vertices_.size();
	vertices_.push_back(id);
}

void Graph::beginAddingOutEdges(int id)
{
	currentIdx_ = idToIdx_[id];
	vertices_[currentIdx_].outEdgesBegin = edges_.size();
}

void Graph::beginAddingInEdges(int id)
{
	currentIdx_ = idToIdx_[id];
	vertices_[currentIdx_].inEdgesBegin = edges_.size();
}

void Graph::endAddingOutEdges()
{
	Vertex &v = vertices_[currentIdx_];
	v.outEdgesEnd = edges_.size();
	outEdgeCount_ += v.outEdgesEnd - v.outEdgesBegin;
	std::sort(edges_.data() + v.outEdgesBegin, edges_.data() + v.outEdgesEnd);
}

void Graph::endAddingInEdges()
{
	Vertex &v = vertices_[currentIdx_];
	v.inEdgesEnd = edges_.size();
	std::sort(inBegin(v.id), inEnd(v.id));
}

void Graph::addEdge(int v)
{
	edges_.push_back(v);
}

Graph::Vertex &Graph::addForeignVertexIfNecessary(int id)
{
	std::map<int, int>::iterator it = idToIdx_.find(id);
	if (it != idToIdx_.end()) {
		return foreignVertices_[it->second];
	}
	idToIdx_[id] = foreignVertices_.size();
	foreignVertices_.push_back(id);
	return foreignVertices_.back();
}

void Graph::buildForeignVertices()
{
	std::map<int, std::vector<int> > out;
	std::map<int, std::vector<int> > in;
	for (size_t i = 0; i < vertices_.size(); ++i) {
		Vertex &vertex = vertices_[i];
		for (int e = vertex.outEdgesBegin; e != vertex.outEdgesEnd; ++e) {
			if (isForeign(edges_[e])) {
				in[edges_[e]].push_back(vertex.id);
			}
		}
		for (int e = vertex.inEdgesBegin; e != vertex.inEdgesEnd; ++e) {
			if (isForeign(edges_[e])) {
				out[edges_[e]].push_back(vertex.id);
			}
		}
	}
	
	for (std::map<int, std::vector<int> >::iterator it = out.begin(); it != out.end(); it++) {
		Vertex &vertex = addForeignVertexIfNecessary(it->first);
		std::sort(it->second.begin(), it->second.end());
		vertex.outEdgesBegin = edges_.size();
		edges_.insert(edges_.end(), it->second.begin(), it->second.end());
		vertex.outEdgesEnd = edges_.size();
	}
	for (std::map<int, std::vector<int> >::iterator it = in.begin(); it != in.end(); it++) {
		Vertex &vertex = addForeignVertexIfNecessary(it->first);
		std::sort(it->second.begin(), it->second.end());
		vertex.inEdgesBegin = edges_.size();
		edges_.insert(edges_.end(), it->second.begin(), it->second.end());
		vertex.inEdgesEnd = edges_.size();
	}
}

