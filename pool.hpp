#pragma once

#include <vector>

template <typename T>
class Pool {
public:
	Pool() {}

	~Pool() {
		for (size_t i = 0; i < allObjects_.size(); ++i) {
			delete allObjects_[i];
		}
	}

	void manage(T *obj) {
		allObjects_.push_back(obj);
		freeObjects_.push_back(obj);
	}

	T *acquire() {
		if (freeObjects_.empty()) {
			return NULL;
		}
		T *obj = freeObjects_.back();
		freeObjects_.pop_back();
		return obj;
	}

	void release(T *obj) {
		freeObjects_.push_back(obj);
	}

	void releaseAll() {
		freeObjects_ = allObjects_;
	}

private:
	std::vector<T*> allObjects_;
	std::vector<T*> freeObjects_;

	Pool(const Pool &);
	Pool &operator=(const Pool &);

};

