#pragma once

#include <vector>
#include <map>
#include <cstdio>

namespace Graph_detail {

struct Vertex {
	Vertex(int id) :
		id(id),
		inEdgesBegin(0),
		inEdgesEnd(0),
		outEdgesBegin(0),
		outEdgesEnd(0)
	{}

	int id;
	int inEdgesBegin;
	int inEdgesEnd;
	int outEdgesBegin;
	int outEdgesEnd;
};

} //namespace Graph_detail

class Graph {
public:
	typedef Graph_detail::Vertex Vertex;
	
	Graph(int rank);
	~Graph();

	void addVertex(int id);
	void beginAddingOutEdges(int v);
	void endAddingOutEdges();
	void beginAddingInEdges(int v);
	void endAddingInEdges();
	void addEdge(int v);

	const int *outBegin(int v) const {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.outEdgesBegin;
	}
	
	const int *outEnd(int v) const {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.outEdgesEnd;
	}

	int *outBegin(int v) {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.outEdgesBegin;
	}
	
	int *outEnd(int v) {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.outEdgesEnd;
	}

	const int *inBegin(int v) const {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.inEdgesBegin;
	}

	const int *inEnd(int v) const {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.inEdgesEnd;
	}

	int *inBegin(int v) {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.inEdgesBegin;
	}

	int *inEnd(int v) {
		int idx = idToIdx_.find(v)->second;
		const Vertex &ver = isForeign(v) ? foreignVertices_[idx] : vertices_[idx];
		return edges_.data() + ver.inEdgesEnd;
	}

	void setVertexCount(int n) {
		vertexToProcess_.resize(n, -1);
	}

	int partSize() const {
		return vertices_.size();
	}

	int vertex(int idx) const {
		return vertices_[idx].id;
	}

	int allVertexCount() const {
		return vertexToProcess_.size();
	}

	int outEdgeCount() const {
		return outEdgeCount_;
	}

	void setVertexProcess(int v, int p) {
		vertexToProcess_[v - 1] = p;
	}

	int getVertexProcess(int vertex) const {
		return vertexToProcess_[vertex - 1];
	}

	const std::vector<Vertex> &vertices() const {
		return vertices_;
	}

	bool outEdgeExists(int v1, int v2) const {
		const Vertex &vertex = vertices_[idToIdx_.find(v1)->second];
		return edgeBinSearch(vertex.outEdgesBegin, vertex.outEdgesEnd, v2);
	}

	bool inEdgeExists(int v1, int v2) const {
		const Vertex &vertex = vertices_[idToIdx_.find(v1)->second];
		return edgeBinSearch(vertex.inEdgesBegin, vertex.inEdgesEnd, v2);
	}

	bool isForeign(int v) const {
		return vertexToProcess_[v - 1] != rank_;
	}

	bool isIncident(int v) const {
		return idToIdx_.count(v) > 0;
	}

	void buildForeignVertices();

private:
	int rank_;
	std::vector<int> vertexToProcess_;
	std::vector<Vertex> vertices_;
	std::vector<Vertex> foreignVertices_;
	std::map<int, int> idToIdx_;
	std::vector<int> edges_;
	int currentIdx_;
	int outEdgeCount_;

	Graph(const Graph &);
	Graph &operator=(const Graph &);

	bool edgeBinSearch(int begin, int end, int value) const {
		if (begin == end) {
			return false;
		}
		while (begin + 1 < end) {
			int mid = (begin + end) / 2;
			if (edges_[mid] > value) {
				end = mid;
			} else {
				begin = mid;
			}
		}
		return edges_[begin] == value;
	}

	Vertex &addForeignVertexIfNecessary(int id);

};

