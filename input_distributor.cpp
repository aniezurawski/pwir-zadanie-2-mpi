#include "input_distributor.hpp"

#include <set>
#include <sstream>
#include <iterator>
#include <mpi.h>

InputDistributor::InputDistributor(const std::string &fileName) :
	bufferSender_(kMaxProcessCount, kInputDistributionTag),
	processStats_(kMaxProcessCount),
	nextVertex_(-1)
{
	file_.open(fileName.c_str());
	if (!file_) {
		abort("Cannot open file %s", fileName.c_str());
	}
	vertexToProcess_.reserve(kMaxVertexCount);
}

InputDistributor::~InputDistributor()
{
}

int InputDistributor::distribute(int maxProcessCount)
{
	int vertexCount;
	int vertex;
	int edgeCount;
	std::vector<int> data;
	std::multiset<int, ProcessComparer> processes((ProcessComparer(processStats_)));
	processes.insert(0);
	if (maxProcessCount > 1) {
		processes.insert(1);
	}
	int processCount = processes.size();

	file_ >> vertexCount;
	skipLine();
	vertexToProcess_.resize(vertexCount, -1);
	resetVerticesReader(vertexCount);
	while (tryReadVertexHeader(vertex, edgeCount)) {
		data.resize(edgeCount + 2);
		data[0] = vertex;
		data[1] = edgeCount;
		for (int i = 0; i < edgeCount; ++i) {
			file_ >> data[i + 2];
			skipLine();
		}
		int process = *(processes.begin());
		processes.erase(processes.begin());
		sendDataToProcess(process, data);
		processStats_[process].objects += edgeCount + 1;
		vertexToProcess_[vertex - 1] = process;
		processes.insert(process);
		if (processCount < maxProcessCount && processStats_[process].objects >= kObjectsSoftLimit) {
			processCount = std::min(processCount * 2, maxProcessCount);
			while ((int)processes.size() < processCount) {
				processes.insert(processes.size());
			}
		}
	}
	
	file_ >> vertexCount;
	skipLine();
	resetVerticesReader(vertexCount);
	data.clear();
	data.reserve(vertexCount * vertexCount * 3);
	data.push_back(0);
	data.push_back(processCount);
	data.push_back(vertexCount);
	while (tryReadVertexHeader(vertex, edgeCount)) {
		data.push_back(vertex);
		data.push_back(edgeCount);
		for (int i = 0; i < edgeCount; ++i) {
			int v;
			file_ >> v;
			skipLine();
			data.push_back(v);
		}
	}
	data.push_back(0);
	data.push_back(vertexToProcess_.size());
	for (int i = 0; i < maxProcessCount; ++i) {
		sendDataToProcess(i, data);
	}

	const size_t chunk = 1000;
	for (size_t i = 0; i < vertexToProcess_.size(); i += chunk) {
		for (int j = 0; j < maxProcessCount; ++j) {
			sendDataToProcess(
					j,
					vertexToProcess_.data() + i,
					std::min(chunk, vertexToProcess_.size() - i),
					(i + chunk >= vertexToProcess_.size()));
		}
	}

	bufferSender_.waitForAll();
	return processCount;
}

void InputDistributor::sendDataToProcess(
		int process,
		const std::vector<int> &data,
		bool forceSend)
{
	sendDataToProcess(process, data.data(), data.size(), forceSend);
}

void InputDistributor::sendDataToProcess(
		int process,
		const int *data,
		size_t size,
		bool forceSend)
{
	if (process == kDistributorProcess) {
		inputReceiver_.receivePart(data, size);
	} else {
		bufferSender_.addDataToSend(process, data, size, forceSend);
	}
}

bool InputDistributor::tryReadVertexHeader(int &vertex, int &edgeCount)
{
	do {
		if (nextVertex_ >= 0) {
			break;
		}
		std::string line;
		std::vector<int> integers;
		if (getline(file_, line).eof()) {
			nextVertex_ = 0;
			break;
		}
		std::istringstream is(line);
		integers = std::vector<int>(
				std::istream_iterator<int>(is),
				std::istream_iterator<int>());
		if (integers.empty()) {
			nextVertex_ = 0;
			break;
		}
		if (integers.size() != 2) {
			abort("Incorrect input. Read '%s', expected 2 integers.");
		}
		vertex = integers.front();
		edgeCount = integers.back();
		verticesBitMap_[vertex - 1] = true;
		return true;
	} while (false);

	while (nextVertex_ < (int)verticesBitMap_.size() && verticesBitMap_[nextVertex_]) {
		++nextVertex_;
	}
	if (nextVertex_ >= (int)verticesBitMap_.size()) {
		return false;
	}
	verticesBitMap_[nextVertex_] = true;
	vertex = ++nextVertex_;
	edgeCount = 0;
	return true;
}

