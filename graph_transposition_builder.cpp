#include "graph_transposition_builder.hpp"

GraphTranspositionBuilder::GraphTranspositionBuilder(int processCount) :
	processCount_(processCount),
	sender_(processCount, kInputDistributionTag),
	receiver_(kInputDistributionTag),
	edgeSource_(processCount, -1)
{
}

void GraphTranspositionBuilder::transpose(Graph &graph)
{
	graph_ = &graph;
	for (int i = 0; i < processCount_; ++i) {
		if (i == gRank) {
			send();
		} else {
			receive(i);
		}
	}
	addEdges();
	sender_.waitForAll();
}

void GraphTranspositionBuilder::receive(int process)
{
	std::vector<int> *buffer = NULL;
	do {
		if (buffer) {
			delete buffer;
			buffer = NULL;
		}
		receiver_.initReceive(process);
		receiver_.receiveData(&buffer);
	} while (processData(process, *buffer));
	delete buffer;
}

void GraphTranspositionBuilder::addEdges()
{
	const std::vector<Graph::Vertex> &vertices = graph_->vertices();
	for (size_t i = 0; i < vertices.size(); ++i) {
		int vertex = vertices[i].id;
		graph_->beginAddingInEdges(vertex);
		std::vector<int> &edges = edges_[vertex];
		for (size_t j = 0; j < edges.size(); ++j) {
			graph_->addEdge(edges[j]);
		}
		graph_->endAddingInEdges();
	}
}

void GraphTranspositionBuilder::send()
{
	std::vector<int> tmp(2);
	const std::vector<Graph::Vertex> &vertices = graph_->vertices();
	for (size_t i = 0; i < vertices.size(); ++i) {
		int vertex = vertices[i].id;
		edges_[vertex].reserve(50);
		for (const int *edge = graph_->outBegin(vertex); edge != graph_->outEnd(vertex); edge++) {
			int dest = graph_->getVertexProcess(*edge);
			if (dest == gRank) {
				edges_[*edge].push_back(vertex);
			} else {
				tmp[0] = vertex;
				tmp[1] = *edge;
				sender_.addDataToSend(dest, tmp);
			}
		}
	}
	for (int i = 0; i < processCount_; ++i) {
		if (i != gRank) {
			sender_.addDataToSend(i, std::vector<int>(1, 0), true);
		}
	}
}

bool GraphTranspositionBuilder::processData(int process, const std::vector<int> &data)
{
	for (size_t i = 0; i < data.size(); ++i) {
		int v = data[i];
		if (v == 0) {
			return false;
		}
		if (edgeSource_[process] < 0) {
			edgeSource_[process] = v;
		} else {
			int source = -1;
			std::swap(source, edgeSource_[process]);
			edges_[v].push_back(source);
		}
	}
	return true;
}

