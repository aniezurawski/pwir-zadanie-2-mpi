#pragma once

#include "common.hpp"

#include <string.h>

template<typename T>
class IncrementalBuffer {
public:
	/*IncrementalBuffer() :
		buffer_(NULL), size_(0), capacity(0) {}
*/
	IncrementalBuffer(size_t capacity) :
		buffer_(new T[capacity]), size_(0), capacity_(capacity) {}
	
	~IncrementalBuffer() {
		if (buffer_) {
			delete[] buffer_;
		}
	}

	/*void init(int capacity) {
		assert_abort(!buffer_);
		buffer_ = new T[capacity];
		size_ = 0;
		capacity_ = capacity;
	}*/

	T *data() {
		return buffer_;
	}

	size_t freeSpace() const {
		return capacity_ - size_;
	}

	bool full() const {
		return size_ == capacity_;
	}

	void clear() {
		size_ = 0;
	}

	void push(const T *data, size_t size) {
		massert(freeSpace() >= size);
		memcpy(buffer_ + size_, data, size * sizeof(T));
		size_ += size;
	}

	size_t size() const {
		return size_;
	}

private:
	T *buffer_;
	size_t size_;
	size_t capacity_;

	IncrementalBuffer(const IncrementalBuffer &);
	IncrementalBuffer &operator=(const IncrementalBuffer &);

};

