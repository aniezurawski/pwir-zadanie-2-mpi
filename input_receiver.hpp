#pragma once

#include "pattern.hpp"
#include "graph.hpp"
#include "buffer_receiver.hpp"

#include <vector>
#include <memory.h>
#include <cstdio>

class InputReceiver {
	enum State {
		kReadGraphVertex,
		kReadGraphEdgeCount,
		kReadGraphEdges,
		kReadProcessCount,
		kReadPatternVertexCount,
		kReadPatternVertex,
		kReadPatternEdgeCount,
		kReadPatternEdges,
		kReadProcessMapSize,
		kReadProcessMapData,
		kReadEnd
	};

public:
	InputReceiver();
	~InputReceiver();

	int receiveByMPI();
	bool receivePart(const int *data, size_t size);

	Graph *graph() const {
		return graph_;
	}

	Pattern *pattern() const {
		return pattern_;
	}

private:
	int processCount_;
	Graph *graph_;
	Pattern *pattern_;
	State state_;
	BufferReceiver<int, kDistributionBlockSize> receiver_;

	int vertexId_;
	int dataToRead_;

	void readGraphVertex(const int *&pointer, const int *end) {
		vertexId_ = *(pointer++);
		if (vertexId_ == 0) {
			state_ = kReadProcessCount;
		} else {
			graph_->addVertex(vertexId_);
			graph_->beginAddingOutEdges(vertexId_);
			state_ = kReadGraphEdgeCount;
		}
	}

	void readGraphEdgeCount(const int *&pointer, const int *end) {
		dataToRead_ = *(pointer++);
		state_ = kReadGraphEdges;
	}

	void readGraphEdges(const int *&pointer, const int *end) {
		while (pointer < end && dataToRead_ > 0) {
			int e = *(pointer++);
			graph_->addEdge(e);
			--dataToRead_;
		}
		if (dataToRead_ == 0) {
			graph_->endAddingOutEdges();
			state_ = kReadGraphVertex;
		}
	}
	
	void readProcessCount(const int *&pointer, const int *end) {
		processCount_ = *(pointer++);
		state_ = kReadPatternVertexCount;
	}

	void readPatternVertexCount(const int *&pointer, const int *end) {
		vertexId_ = *(pointer++);
		pattern_ = new Pattern(vertexId_);
		state_ = kReadPatternVertex;
	}

	void readPatternVertex(const int *&pointer, const int *end) {
		vertexId_ = *(pointer++);
		if (vertexId_ == 0) {
			state_ = kReadProcessMapSize;
		} else {
			state_ = kReadPatternEdgeCount;
		}
	}

	void readPatternEdgeCount(const int *&pointer, const int *end) {
		dataToRead_ = *(pointer++);
		state_ = kReadPatternEdges;
	}

	void readPatternEdges(const int *&pointer, const int *end) {
		while (pointer < end && dataToRead_ > 0) {
			int e = *(pointer++);
			pattern_->addEdge(vertexId_, e);
			--dataToRead_;
		}
		if (dataToRead_ == 0) {
			state_ = kReadPatternVertex;
		}
	}

	void readProcessMapSize(const int *&pointer, const int *end) {
		dataToRead_ = *(pointer++);
		graph_->setVertexCount(dataToRead_);
		vertexId_ = 1;
		state_ = kReadProcessMapData;
	}

	void readProcessMapData(const int *&pointer, const int *end){
		while (pointer < end && dataToRead_ > 0) {
			int process = *(pointer++);
			graph_->setVertexProcess(vertexId_, process);
			++vertexId_;
			--dataToRead_;
		}
		if (dataToRead_ == 0) {
			state_ = kReadEnd;
		}
	}

};

