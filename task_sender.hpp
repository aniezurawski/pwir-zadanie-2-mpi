#pragma once

#include "buffer_sender.hpp"
#include "common.hpp"
#include "task.hpp"

template <size_t BufferSize, size_t BlockSize>
class TaskSender {
public:
	TaskSender(int processCount, int tag) :
		processCount_(processCount),
		process_(0),
		sent_(0),
		task_(),
		bufferSender_(processCount, tag) {
		task_.reserve(kMaxPatternSize);
	}

	void addTask(int process, const Task &task, bool forceSend) {
		if (sent_ < task_.size()) {
			bufferSender_.addDataToSend(
					process, task_.data() + sent_, task_.size() - sent_, false);
		}
		task.serialize(task_);
		bufferSender_.addDataToSend(
				process, task_.data(), task_.size(), forceSend);
		sent_ = task_.size();
	}

	bool tryAddTask(int process, const Task &task) {
		if (sent_ == task_.size()) {
			task.serialize(task_);
			sent_ = 0;
			process_ = process;
		}
		return processData();
	}

	bool processData() {
		sent_ += bufferSender_.tryAddDataToSend(
				process_,
				task_.data() + sent_,
				task_.size() - sent_,
				false);
		return sent_ == task_.size();
	}

	void forceSend() {
		for (int i = 0; i < processCount_; ++i) {
			bufferSender_.forceSend(i);
		}
	}

	const std::vector<MPI_Request> getRequests() const {
		return bufferSender_.getRequests();
	}

	void reportEndRequest(int idx, const MPI_Status &status) {
		bufferSender_.reportEndRequest(idx, status);
	}

	void deleteEndedRequests() {
		bufferSender_.deleteEndedRequests();
	}

	const std::vector<int> &destProcess() const {
		return bufferSender_.destProcess();
	}

private:
	int processCount_;
	int process_;
	size_t sent_;
	std::vector<int> task_;
	BufferSender<int, BufferSize, BlockSize> bufferSender_;

};
