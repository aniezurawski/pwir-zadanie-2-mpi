#pragma once

#include "common.hpp"
#include "task.hpp"
#include "task_receiver.hpp"
#include "task_sender.hpp"
#include "pattern.hpp"

#include <cstdio>
#include <set>

class ResultCollector {
	/*struct Comparer {
		bool operator()(
				const std::pair<Task *, int> &a,
				const std::pair<Task *, int> &b) {
			return (*a.first) < (*b.first);
		}
	};*/

public:
	static const int kPoolSize = 1 << 22;
	static const int kBlockSize = 1 << 20;

	ResultCollector(
			int processcount,
			const Pattern &pattern,
			int rank,
			const std::vector<Task> &resultPart);

	void collect(FILE *output);

private:
	int processCount_;
	int rank_;
	int ended_;
	size_t resultPartPos_;
	const Pattern &pattern_;
	const std::vector<Task> &resultPart_;
	TaskSender<kPoolSize, kBlockSize> sender_;
	TaskReceiver<kBlockSize> receiver_;
	//std::set<std::pair<Task *, int>, Comparer> tasks_;

	Task *receive();

};

