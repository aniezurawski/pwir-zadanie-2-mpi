#pragma once

#include <vector>
#include <mpi.h>

class RequestSet {
public:
	RequestSet() : limits_(1, 0) {}

	int addGroup(const std::vector<MPI_Request> &req) {
		requests_.insert(requests_.end(), req.begin(), req.end());
		limits_.push_back(requests_.size());
		return limits_.size() - 2;
	}

	std::pair<int, int> getGroupAndIndex(size_t pos) {
		size_t g;
		for (g = 0; limits_[g + 1] <= pos; ++g) {
			massert(g + 2 < limits_.size());
		}
		return std::pair<int, int>(g, pos - limits_[g]);
	}

	MPI_Request *data() {
		return requests_.data();
	}

	size_t size() const {
		return requests_.size();
	}

private:
	std::vector<MPI_Request> requests_;
	std::vector<size_t> limits_;

};

