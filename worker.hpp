#pragma once

#include "task_sender.hpp"
#include "task_receiver.hpp"
#include "common.hpp"
#include "graph.hpp"
#include "pattern.hpp"

#include "progress_data.hpp"

#include <set>
#include <algorithm>
#include <cstdlib>

class TaskInfo {
public:
	TaskInfo(int processCount) :
		task_(NULL),
		taskToSend_(NULL),
		progress_(0),
		progressData_(NULL) {
		dest_.reserve(processCount);
	}

	~TaskInfo() {
		massert(!task_);
	}

	const Task *task() const {
		return task_;
	}

	void clear() {
		massert(!taskToSend_);
		massert(dest_.empty());
		delete task_;
		task_ = NULL;
		progress_ = 0;
	}

	void set(const Task *task) {
		massert(!task_);
		task_ = task;
	}

	const Task *taskToSend() const {
		return taskToSend_;
	}

	void clearTaskToSend() {
		delete taskToSend_;
		taskToSend_ = NULL;
	}

	void setTaskToSend(const Task *task) {
		massert(!taskToSend_);
		taskToSend_ = task;
	}

	void addDestinationProcess(int id) {
		massert(taskToSend_);
		dest_.push_back(id);
	}

	int nextDestinationProcess() const {
		return dest_.back();
	}

	void removeDestinationProcess() {
		dest_.pop_back();
	}

	bool destinationProcessExists() const {
		return !dest_.empty();
	}

	void shuffleDestinations() {
		static int seed = 17;
		seed *= 17;
		std::srand(seed);
		std::random_shuffle(dest_.begin(), dest_.end());
	}

	int progress() const {
		return progress_;
	}

	void incProgress(int v = 1) {
		progress_ += v;
	}

	void setProgress(int progress) {
		progress_ = progress;
	}

	ProgressData *progressData() const {
		return progressData_;
	}

	void setProgressData(ProgressData *pd) {
		progressData_ = pd;
	}

private:
	const Task *task_;
	const Task *taskToSend_;
	std::vector<int> dest_;
	int progress_;
	ProgressData *progressData_;

};

class Worker {
public:
	static const size_t kTaskPoolSize = 1 << 20;
	static const size_t kTaskBlockSize = 1 << 10;

	Worker(int processCount, int procId, const Graph &graph, const Pattern &pattern);
	~Worker();
	void work(std::vector<Task> &result);

private:
	int processCount_;
	int procId_;
	const Graph &graph_;
	const Pattern &pattern_;
	std::vector<TaskSender<kTaskPoolSize, kTaskBlockSize> *> taskSenders_;
	std::vector<TaskReceiver<kTaskBlockSize> *> taskReceivers_;
	std::vector<TaskInfo> tasks_;
	MPI_Request *idleTokenReq_;
	int idleTokenBuffer_;
	std::vector<Task> *matches_;

	bool tryReceiveTask(int level);
	bool trySendTask(int level);
	void processTask(int level);

	std::vector<MPI_Request> getIdleTokenRecvReq();
	std::vector<MPI_Request> getIdleTokenSendReq(int tokenCounter);

	Task *findNewMatch(TaskInfo &taskInfo, int level);

	void addDestinationProcesses(const Task &task, TaskInfo &taskInfo);

};

