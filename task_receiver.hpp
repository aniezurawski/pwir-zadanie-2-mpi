#pragma once

#include "task.hpp"
#include "common.hpp"
#include "buffer_receiver.hpp"

#include <algorithm>

template <size_t BlockSize>
class TaskReceiver {
public:
	TaskReceiver(int processCount, int patternSize, int tag, int rank) :
		processCount_(processCount),
		patternSize_(patternSize),
		rank_(rank),
		receiver_(tag)
	{
		for (int p = 0; p < processCount_; ++p) {
			task_[p].reserve(kMaxPatternSize);
			buffer_[p] = NULL;
			processed_[p] = 0;
			if (p != rank_) {
				receiver_.initReceive(p);
			}
		}
	}

	Task *receiveTask()
	{
		for (int i = 0; i < processCount_; ++i) {
			Task *task = processFrom(i);
			if (task) {
				return task;
			}
		}

		do {
			std::vector<int> *buffer;
			int source = receiver_.receiveData(&buffer);
			receiver_.initReceive(source);
			buffer_[source] = buffer;
			Task *task = processFrom(source);
			if (task) {
				return task;
			}
		} while (true);
	}

	Task *tryReceiveTask()
	{
		for (int i = 0; i < processCount_; ++i) {
			Task *task = processFrom(i);
			if (task) {
				return task;
			}
		}

		std::vector<int> *buffer;
		int source = receiver_.tryReceiveData(&buffer);
		if (source < 0) {
			return NULL;
		}
		receiver_.initReceive(source);
		buffer_[source] = buffer;
		Task *task = processFrom(source);
		if (task) {
			return task;
		}
		return NULL;
	}

	const std::vector<MPI_Request> &getRequests() const {
		return receiver_.getRequests();
	}

	void reportEndRequest(int idx, MPI_Status &status) {
		receiver_.reportEndRequest(idx, status);
	}

	void deleteEndedRequests() {
		receiver_.deleteEndedRequests();
	}

private:
	int processCount_;
	int patternSize_;
	int rank_;
	BufferReceiver<int, BlockSize> receiver_;
	std::vector<int> *buffer_[kMaxProcessCount];
	size_t processed_[kMaxProcessCount];
	std::vector<int> task_[kMaxProcessCount];
	std::vector<int> endedRequests_;

	Task *processFrom(int p) {
		std::vector<int> &task = task_[p];
		int toProcess = std::min(
				patternSize_ - task.size(),
				(buffer_[p] == NULL ? 0 : buffer_[p]->size()) - processed_[p]);
		if (toProcess == 0) {
			return NULL;
		}
		task.insert(
				task.end(),
				buffer_[p]->data() + processed_[p],
				buffer_[p]->data() + processed_[p] + toProcess);
		processed_[p] += toProcess;
		if (processed_[p] == buffer_[p]->size()) {
			delete buffer_[p];
			buffer_[p] = NULL;
			processed_[p] = 0;
		}
		if ((int)task.size() == patternSize_) {
			Task *newTask = new Task(patternSize_);
			newTask->deserialize(task);
			task.clear();
			return newTask;
		}
		return NULL;
	}

};

