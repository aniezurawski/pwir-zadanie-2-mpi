#pragma once

#include <mpi.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef PROCESS_NUMBER
	#define PROCESS_NUMBER 512
#endif

const int kMaxProcessCount = PROCESS_NUMBER;
const int kMaxVertexCount = 10000000;
const int kMaxPatternSize = 10;
const int kDistributorProcess = 0;

const int kDistributionPoolSize = 1 << 23;
const int kDistributionBlockSize = 1 << 13;

const int kInputDistributionTag = 1;
const int kIdleTokenTag = 2;
const int kResultCollectionTag = 3;
const int kTaskLevelTag[kMaxPatternSize] = { 100, 101, 102, 103, 104, 105, 106,  107, 108, 109 };

extern int gRank;

void abort(const char* format, ...);

#define massert(cond) \
	if (!(cond)) { \
		abort("assertion '" #cond "' failed"); \
	}

template <typename T>
struct MpiType;

template <>
struct MpiType<int> {
	static MPI_Datatype value() {
		return MPI_INT;
	}
};

